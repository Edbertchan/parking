package swipe.android.DatabaseHelpers;

import com.edbert.library.database.DatabaseHelperInterface;

public class LocationDatabaseHelper implements DatabaseHelperInterface {

	public static final String AUTHORITY = "";

	public static final String TABLE_NAME = "locations";

	public static final String COLUMN_HIDDEN_ID = "HIDDEN_id";


	public static final String COLUMN_LOCATION_LATITUDE = "latitude";
	public static final String COLUMN_LOCATION_LONGITUDE = "longitude";
/*
	public static final String COLUMN_DATE = "DATE";
	public static final String COLUMN_LOCATION_NAME = "NAME";
	public static final String COLUMN_PRICE_PER_HOUR = "RATE";
	public static final String COLUMN_SPOTS_TAKEN = "USED_SPOT";
	public static final String COLUMN_SPOTS_AVAILABLE = "TOTAL_SPOT";

	*/
	public static final String COLUMN_LAST_TAKEN ="COLUMN_LAST_TAKEN";
	
	public static final String COLUMN_DISTANCE ="COLUMN_DISTANCE";
	public static final String[] COLUMNS = { COLUMN_HIDDEN_ID,
			COLUMN_LOCATION_LATITUDE, COLUMN_LOCATION_LONGITUDE,
			
			 COLUMN_DISTANCE,COLUMN_LAST_TAKEN };

	public static final String TABLE_CREATE_ROUTES = "CREATE TABLE "
			+ TABLE_NAME + "(" + COLUMN_HIDDEN_ID + " INTEGER NOT NULL primary key autoincrement, "
			
			+COLUMN_DISTANCE + " DOUBLE, "
			+COLUMN_LAST_TAKEN + " BIGINT, "
			+ COLUMN_LOCATION_LATITUDE + " DOUBLE, " 

			+ COLUMN_LOCATION_LONGITUDE + " DOUBLE);";

	@Override
	public String getTableName() {
		return TABLE_NAME;
	}

	@Override
	public String getColumnID() {
		return this.COLUMN_HIDDEN_ID;
	}

	@Override
	public String getCreateTableCommand() {
		return TABLE_CREATE_ROUTES;
	}

	@Override
	public String[] getColumns() {
		return COLUMNS;
	}
}
