package gov.sfmta.sfpark;

public interface Refreshable{
	public void onRefresh();
}