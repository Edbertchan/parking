package gov.sfmta.sfpark.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class SmartParkSyncService extends Service {

    private static final Object sSyncAdapterLock = new Object();

    private static SmartParkSyncAdapter sSyncAdapter = null;

    @Override
    public void onCreate() {
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null) {
                sSyncAdapter = new SmartParkSyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}