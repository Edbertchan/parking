package gov.sfmta.sfpark;

import java.util.Date;
import java.util.Set;

import swipe.android.DatabaseHelpers.LocationDatabaseHelper;
import gov.sfmta.sfpark.core.BaseMapFragment;
import gov.sfmta.sfpark.core.ClusterMask;
import gov.sfmta.sfpark.requests.LocationParkingRequest;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.edbert.library.sendRequest.SendRequestStrategyManager;
import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager.OnClusterClickListener;
import com.google.maps.android.clustering.ClusterManager.OnClusterItemClickListener;

public class PriceFragment extends BaseMapFragment {
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setHasOptionsMenu(false);

	}

	@Override
	public void onLoadFinished(Loader<Cursor> arg0, Cursor arg1) {
		// should clear before loading
		mMap.clear();
		mClusterManager.clearItems();

		int locationCount = 0;
		double lat = 0;
		double lng = 0;
		// float zoom = 0;

		bc = new LatLngBounds.Builder();

		if (arg1 != null) {

			// Number of locations available in the SQLite database table
			locationCount = arg1.getCount();

			// Move the current record pointer to the first row of the table
			arg1.moveToFirst();

			for (int i = 0; i < locationCount; i++) {

				// pull from needs DB
				lat = arg1
						.getDouble(arg1
								.getColumnIndex(LocationDatabaseHelper.COLUMN_LOCATION_LATITUDE));
				lng = arg1
						.getDouble(arg1
								.getColumnIndex(LocationDatabaseHelper.COLUMN_LOCATION_LONGITUDE));
				double[] latLng = { lat, lng };// coordinateForMarker(lat, lng);
				this.addLocation(latLng[0], latLng[1]);
				LatLng location = new LatLng(latLng[0], latLng[1]);

				long s = arg1
						.getLong(arg1
								.getColumnIndex(LocationDatabaseHelper.COLUMN_LAST_TAKEN));
				s *= 1000;
				float color = BitmapDescriptorFactory.HUE_RED;
				long diff = new Date().getTime() - s;
				Log.d("Diff", String.valueOf(diff));

				if (diff < 5 * 60 * 1000) {
					color = BitmapDescriptorFactory.HUE_GREEN;
					// Log.d("one is green", "one is green");
				} else if (diff < 10 * 60 * 1000 && diff >= 5 * 60 * 1000) {
					color = BitmapDescriptorFactory.HUE_YELLOW;
					// Log.d("one is yellow", "one is yellow");
				} else {
					// Log.d("NOPE", "Nope");
				}
				// title += (": (" + takenSpots + "/" + availbleSpots + ")   ");
				int hiddenID = arg1
						.getInt(arg1
								.getColumnIndex(LocationDatabaseHelper.COLUMN_HIDDEN_ID));
				drawMarker(location, color, String.valueOf(hiddenID));
				bc.include(location);
				arg1.moveToNext();
				// fx zoom
			}
			// bc needs to include your current location as well as the default
		}

		if (isMapLoaded) {
			if(SessionManager.getInstance(this.getActivity()).hasCustomLocation()){
				//searchText.setText((String) o);
				double lat1 = Double.valueOf(SessionManager.getInstance(this.getActivity()).getCustomSearchLatitude());
				double lng1 = Double.valueOf(SessionManager.getInstance(this.getActivity()).getCustomSearchLongitude());
				bc.include(new LatLng(lat1,
						lng1));
				bc.include(new LatLng(lat1+.005,
						lng1+.005));
				if (isMapLoaded)
					mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 50));
			}else {
				super.goToOwnLocation();
			}
		}
	}

	@Override
	public void reloadData() {
		// should clear before loading
		mMap.clear();

		Cursor arg1 = generateCursor();

		onLoadFinished(null, arg1);
	}

	@Override
	public void setSourceRequestHelper() {
		helpers.add(SendRequestStrategyManager
				.getHelper(LocationParkingRequest.class));// new
															// NeedsDetailsRequest(this.getActivity(),
															// JsonExploreResponse.class);
	}

	// obsolete since we're using a cursor callback
	@Override
	public CursorLoader generateCursorLoader() {

		String allActiveSearch = "";
		String[] activeStates = null;
		CursorLoader cursorLoader = new CursorLoader(
				this.getActivity(),
				SmartParkContentProvider
						.contentURIbyTableName(LocationDatabaseHelper.TABLE_NAME),
				LocationDatabaseHelper.COLUMNS, allActiveSearch, activeStates,
				LocationDatabaseHelper.COLUMN_HIDDEN_ID + " DESC");

		return cursorLoader;
	}

	@Override
	public String syncStartedFlag() {
		return MESSAGES_START_FLAG;
	}

	@Override
	public String syncFinishedFlag() {
		return MESSAGES_FINISH_FLAG;
	}

	private ClusterMask clickedClusterItem;
	private Cluster<ClusterMask> clickedCluster;

	@Override
	protected void setUpClusterer() {
		super.setUpClusterer();
		mClusterManager
				.setOnClusterClickListener(new OnClusterClickListener<ClusterMask>() {
					@Override
					public boolean onClusterClick(Cluster<ClusterMask> cluster) {
						clickedCluster = cluster;
						return false;
					}
				});

		mClusterManager
				.setOnClusterItemClickListener(new OnClusterItemClickListener<ClusterMask>() {
					@Override
					public boolean onClusterItemClick(ClusterMask item) {
						clickedClusterItem = item;
						return false;
					}
				});
	}

	@Override
	protected void attachInfoWindowAdapter() {

		mMap.setInfoWindowAdapter(new InfoWindowAdapter() {
			private View myContentView = PriceFragment.this.getActivity()
					.getLayoutInflater().inflate(R.layout.needs_marker, null);

			@Override
			public View getInfoContents(Marker marker) {
				/*
				 * View myContentView = PriceFragment.this.getActivity()
				 * .getLayoutInflater() .inflate(R.layout.needs_marker, null);
				 * TextView needs_title = ((TextView) myContentView
				 * .findViewById(R.id.needs_task));
				 * needs_title.setText(marker.getTitle()); TextView needs_price
				 * = ((TextView) myContentView .findViewById(R.id.needs_price));
				 * String snip = marker.getSnippet(); String priceSubstring =
				 * snip.substring(1, snip.lastIndexOf(",")); double roundOff =
				 * Math.round(Double.valueOf(priceSubstring) * 100.0) / 100.0;
				 * needs_price.setText("$" + String.valueOf(roundOff)); return
				 * myContentView;
				 */
				return null;
			}

			@Override
			public View getInfoWindow(Marker marker) {
				// TODO Auto-generated method stub

				TextView needs_title = ((TextView) myContentView
						.findViewById(R.id.needs_task));
				TextView distance = ((TextView) myContentView
						.findViewById(R.id.distance));

				distance.setVisibility(View.GONE);
				TextView needs_price = ((TextView) myContentView
						.findViewById(R.id.needs_price));
				RelativeTimeTextView timeAvail = (RelativeTimeTextView) myContentView
						.findViewById(R.id.time_ago);
				timeAvail.setVisibility(View.GONE);
				needs_title.setText("There are multiple spots here!");

				if (clickedClusterItem != null) {
					// query the data
					Cursor c = generateClusterCursor(clickedClusterItem.getId());
					c.moveToFirst();
					if (c.getCount() == 0) {
						// Log.d("NO ITEMS", "NO ITEMS");
						// get here if we're a cluster
						Log.d("HI", "HI");
						c.close();
						return myContentView;
					}
					needs_title.setText("Click for directions");

					// problem is processing. this should only happen once.
					long s = c.getLong(c
							.getColumnIndex(LocationDatabaseHelper.COLUMN_LAST_TAKEN));

					long NOW = new Date().getTime() / 1000;

					timeAvail.setReferenceTime(s * 1000);
					/*
					 * String title = c.getString(c
					 * .getColumnIndex(LocationDatabaseHelper
					 * .COLUMN_LOCATION_NAME)); double price = c.getDouble(c
					 * .getColumnIndex
					 * (LocationDatabaseHelper.COLUMN_PRICE_PER_HOUR)); double
					 * roundOff = Math.round(Double.valueOf(price) * 100.0) /
					 * 100.0;
					 */
					double distance_query = c.getDouble(c
							.getColumnIndex(LocationDatabaseHelper.COLUMN_DISTANCE));
					double roundOff = Math.round(Double.valueOf(distance_query) * 100.0) / 100.0;
					distance.setText(String.valueOf(roundOff) + " miles away");
					timeAvail.setVisibility(View.VISIBLE);
					distance.setVisibility(View.VISIBLE);
					// needs_title.setText(title);
					// needs_price.setText(String.valueOf(roundOff));
					c.close();
				}
				return myContentView;
			}
		});

	}

	public Cursor generateClusterCursor(String id) {
		String selectionClause = LocationDatabaseHelper.COLUMN_HIDDEN_ID
				+ " = ?";
		String[] mSelectionArgs = { "" };
		mSelectionArgs[0] = id;
		Cursor c = this
				.getActivity()
				.getContentResolver()
				.query(

				SmartParkContentProvider
						.contentURIbyTableName(LocationDatabaseHelper.TABLE_NAME),
						LocationDatabaseHelper.COLUMNS, selectionClause,
						mSelectionArgs,
						LocationDatabaseHelper.COLUMN_HIDDEN_ID + " DESC");
		return c;

	}

	@Override
	protected void attachInfoWindowClickListener() {
		/*
		 * mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {
		 * 
		 * @Override public void onInfoWindowClick(Marker marker) { Intent
		 * intent = new Intent(DiscoverMapViewFragment.this .getActivity(),
		 * NeedsDetailsActivity.class); Bundle extras = new Bundle(); Cursor c =
		 * generateCursor(); String snip = marker.getSnippet();
		 * 
		 * 
		 * int position = Integer.valueOf(snip.substring( snip.lastIndexOf(",")
		 * + 1, snip.length())); c.moveToPosition(position); String need_id =
		 * c.getString(c .getColumnIndex(NeedsDetailsDatabaseHelper.COLUMN_ID));
		 * extras.putString("id", need_id); intent.putExtras(extras);
		 * 
		 * startActivity(intent); }
		 * 
		 * });
		 */
	}

}