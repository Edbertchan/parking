package gov.sfmta.sfpark;

import java.util.Set;

import gov.sfmta.sfpark.core.ActivityContainer;
import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

public class MainScreenActivity extends ActivityContainer {
	TextView searchText;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		final ViewGroup actionBarLayout = (ViewGroup) this.getLayoutInflater()
				.inflate(R.layout.actionbar_searchbar, null);
		searchText = (TextView) actionBarLayout.findViewById(R.id.search_text);

		searchText.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainScreenActivity.this,
						SearchActivity.class);
				startActivity(intent);
			}

		});
		ImageButton bluetoothPair = (ImageButton) actionBarLayout
				.findViewById(R.id.link_bluetooth);
		bluetoothPair.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainScreenActivity.this,
						ListOfPairable.class);
				startActivity(intent);
			}

		});
		final ActionBar actionBar = this.getActionBar();
		actionBar.setDisplayShowCustomEnabled(true);
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setDisplayHomeAsUpEnabled(false);
		// Set up your ActionBar
		actionBar.setCustomView(actionBarLayout);

		// searchTerm = (TextView)
		// actionBarLayout.findViewById(R.id.search_text);

		Button b = (Button) findViewById(R.id.refresh_items);
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				/*
				 * LocationParkingRequest rq = new LocationParkingRequest(
				 * MainScreenActivity.this);
				 */
				// rq.writeToDatabase(null, MainScreenActivity.this, null);
				// int pos =
				// MainScreenActivity.this.getSupportActionBar().getSelectedTab().getPosition();
				// String tag = createTag(super.mapFragList.getValue(pos));
				Refreshable refreshing = null;
				if (f instanceof Refreshable) {
					refreshing = (Refreshable) f;
				}
				if (refreshing != null) {
					// /f.onRefresh();
					refreshing.onRefresh();
				}
			}

		});
	}

	@Override
	public String getCanonicalName() {
		return PriceFragment.class.getCanonicalName();
	}

	@Override
	public void onResume() {
		super.onResume();
		if (SessionManager.getInstance(this).hasCustomLocation()) {
			
			searchText.setText(SessionManager.getInstance(this).getCustomSearchLocationName());

		}else{
			searchText.setText(SessionManager.DEFAULT_STRING_DISPLAY_LOCATION);
		}
	}
}
/*
 * public class MainScreenActivity extends TabsActivityContainer {
 * 
 * @Override protected void onCreate(Bundle savedInstanceState) {
 * super.onCreate(savedInstanceState);
 * setContentView(R.layout.fragment_frame_with_button); ActionBar ab =
 * this.getSupportActionBar();
 * 
 * }
 * 
 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
 * menu; this adds items to the action bar if it is present.
 * getMenuInflater().inflate(R.menu.options_menu, menu); //
 * getMenuInflater().inflate(R.layout.options_menu, menu); return true; }
 * 
 * @Override public boolean onOptionsItemSelected(MenuItem item) { // Handle
 * item selection switch (item.getItemId()) { case R.id.m_refresh: // for dev
 * purpose add comment LocationParkingRequest rq = new
 * LocationParkingRequest(this); rq.writeToDatabase(null, this, null); int pos =
 * this.getSupportActionBar().getSelectedTab().getPosition(); String tag =
 * createTag(super.mapFragList.getValue(pos));
 * 
 * Refreshable f = ((Refreshable) getSupportFragmentManager()
 * .findFragmentByTag(tag)); f.onRefresh(); break; // refresh the child fragment
 * } return super.onOptionsItemSelected(item); }
 * 
 * @Override protected void addDefaultFragments() { mapFragList.put("Details",
 * new PriceFragment()); // mapFragList.put("Offers", new
 * AvailabilityFragment()); }
 * 
 * 
 * }
 */
