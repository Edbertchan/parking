package gov.sfmta.sfpark;



import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Window;
import android.widget.TextView;

public class LoadingActivity extends ActionBarActivity {
    public static final boolean DEBUG = true;

    static String VersionText ="error";
    static String VersionCode ="error";
    static String responseString = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getActionBar().hide();
        setContentView(R.layout.splash);
        TextView version = (TextView) findViewById(R.id.textVersion);

        // Set up ActionBar
        ActionBar ab = this.getSupportActionBar();
        ab.setDisplayShowTitleEnabled(false);
        ab.setTitle(R.string.app_name);
        ab.setIcon(R.drawable.logo_header);


        PackageManager pm = getPackageManager();
        try {
            PackageInfo pi = pm.getPackageInfo("gov.sfmta.sfpark", 0);
            VersionText = "Version " + pi.versionName;
            VersionCode = Integer.toString(pi.versionCode);

            if (DEBUG) {
                version.setText(VersionText + "\nBuild " + VersionCode);
            } else {
                version.setText(VersionText);
            }

        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }

        /** create a thread to show splash up to splash time */
        Thread welcomeThread = new Thread() {

                // 2 seconds, enough time to read version number
                long endit = System.currentTimeMillis() + (1400);

                @Override
                public void run() {
                    try {
                        super.run();
                        while (!SmartParkApplication.HAS_LOCATION) {
                       /// 	Log.d("Trying to get loc", "Trying to get loc");
                        }
                        /*if(isBluetooth()) {
                			activateBluetooth();
                		} */
                        
                    } catch (Exception e) {
                        System.out.println("EXc=" + e);
                    } finally {
                      // Called after splash times up. Do some action after splash
                      //  times up. Here we moved to another main activity class
                      
                            startActivity(new Intent(LoadingActivity.this, MainScreenActivity.class));
                       
                        finish();
                    }
                }
            };
        welcomeThread.start();
    }
	
	
}
