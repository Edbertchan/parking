package gov.sfmta.sfpark.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.os.Vibrator;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

public class ConnectionService extends Service {

	public final static String CONNECTING = "Connecting";
	public final static String CONNECTED = "Connected";
	public final static String DISCONNECTED = "Disconnected";

	private static final String TAG = "ConnectionService";

	private AtomicBoolean isConnecting = new AtomicBoolean(false);
	private AtomicBoolean isConnected = new AtomicBoolean(false);

	// A message loop handler to post runnables to in the future:
	private Handler handler = new Handler();

	private Vibrator vibrator;
	private WifiManager.WifiLock wifiLock;
	private PowerManager.WakeLock cpuLock;

	private final RemoteCallbackList<IConnectionChangeCallback> callbackList = new RemoteCallbackList<IConnectionChangeCallback>();

	// For listening to system updates of network & wifi connectivity state:
	private IntentFilter networkChangeIntents = new IntentFilter();
	private final BroadcastReceiver onNetworkChange = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			// ...
		}
	};

	@Override
	public IBinder onBind(Intent arg0) {
		return connectionService;
	}

	@Override
	public void onDestroy() {
		// disconnect(); // TODO
		callbackList.kill(); // unregister all callbacks
		super.onDestroy();
	}

	@Override
	public void onCreate() {
		super.onCreate();
		// wifiLock = wifi().createWifiLock("GarageWifiLock");
		vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

		networkChangeIntents
				.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
		networkChangeIntents.addAction(WifiManager.WIFI_STATE_CHANGED_ACTION);

		PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
		cpuLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
				"Garage CPU lock");

		Log.d(TAG, "onCreate");
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
		Log.d(TAG, "onStart");
	}

	private void disconnect() {
		Log.d(TAG, "disconnect()");
		changeConnectionStatus("foo.com", DISCONNECTED);
	}

	private void connect(String host) {
		Log.d(TAG, "connect()");
		changeConnectionStatus(host, CONNECTING);
	}

	private synchronized void changeConnectionStatus(String host, String status) {
		// Broadcast to all clients the new value.
		final int N = callbackList.beginBroadcast();
		for (int i = 0; i < N; ++i) {
			try {
				callbackList.getBroadcastItem(i).onConnectionStatusChanged(
						host, status);
			} catch (RemoteException e) {
				// The RemoteCallbackList will take care of removing
				// the dead object for us.
			}
		}
		callbackList.finishBroadcast();
	}

	private WifiManager wifi() {
		return (WifiManager) getSystemService(Context.WIFI_SERVICE);
	}

	private final IBusConnectionService.Stub connectionService = new IBusConnectionService.Stub() {

		public boolean isConnected() throws RemoteException {
			return isConnected.get();
		}

		public void startConnecting(String host) throws RemoteException {
			ConnectionService.this.connect(host);
		}

		public void disconnect() throws RemoteException {
			ConnectionService.this.disconnect();
		}

		public void registerCallback(IConnectionChangeCallback callback)
				throws RemoteException {
			if (callback != null) {
				callbackList.register(callback);
			}
			try {
				callback.onConnectionStatusChanged("foo.com", CONNECTING);
			} catch (RemoteException e) {
				// ...
			}
		}

		public void unregisterCallback(IConnectionChangeCallback callback)
				throws RemoteException {
			if (callback != null) {
				callbackList.unregister(callback);
			}
		}

	};
}