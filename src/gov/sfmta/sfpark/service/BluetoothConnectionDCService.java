package gov.sfmta.sfpark.service;

import gov.sfmta.sfpark.SessionManager;
import gov.sfmta.sfpark.SmartParkApplication;
import gov.sfmta.sfpark.core.APIManager;
import gov.sfmta.sfpark.core.Device;
import gov.sfmta.sfpark.json.RemoveBluetoothResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import com.edbert.library.network.AsyncTaskCompleteListener;
import com.edbert.library.network.GetDataWebTask;
import com.edbert.library.utils.MapUtils;

public class BluetoothConnectionDCService extends Service implements
		AsyncTaskCompleteListener {
	public static CountDownTimer timer;
	public static final String TAG = BluetoothConnectionDCService.class
			.getSimpleName();

	private BluetoothAdapter myBluetoothAdapter;
	private Set<BluetoothDevice> pairedDevices;
	private ListView myListView;

	@Override
	public void onCreate() {
		super.onCreate();
		myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (myBluetoothAdapter == null) {
			/*
			 * onBtn.setEnabled(false); offBtn.setEnabled(false);
			 * listBtn.setEnabled(false); findBtn.setEnabled(false);
			 * text.setText("Status: not supported");
			 */

			Toast.makeText(getApplicationContext(),
					"Your device does not support Bluetooth", Toast.LENGTH_LONG)
					.show();
		} else {
			/*
			 * BTArrayAdapter = new ArrayAdapter<String>(this,
			 * android.R.layout.simple_list_item_1);
			 */

			Toast.makeText(getApplicationContext(), "Bluetooth supported",
					Toast.LENGTH_LONG).show();
			IntentFilter intentFilter = new IntentFilter();
			intentFilter.addAction(BluetoothDevice.ACTION_FOUND);
			intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
			intentFilter.addAction(BluetoothDevice.ACTION_ACL_CONNECTED);

			intentFilter.addAction(BluetoothDevice.ACTION_UUID);
			intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
			intentFilter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
			intentFilter
					.addAction(BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED);
			intentFilter.addAction(BluetoothDevice.ACTION_ACL_DISCONNECTED);
			registerReceiver(bReceiver, intentFilter);

			on();
			find();
		}
	}

	public void on() {
		Log.d("On", "on");
		if (!myBluetoothAdapter.isEnabled()) {
			Intent turnOnIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			/*
			 * SmartParkApplication.getInstance().lastAct.startActivityForResult(
			 * turnOnIntent, 1);
			 */
			Toast.makeText(getApplicationContext(), "Bluetooth turned on",
					Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(getApplicationContext(), "Bluetooth is already on",
					Toast.LENGTH_LONG).show();
		}
	}

	final BroadcastReceiver bReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// When discovery finds a device
			BluetoothDevice device = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			// Device found
			if (device == null) {
				return;
			}
			Log.d("MAC", device.getAddress());
			Log.d("action", action);
			// if we have th device, we eject it
			if (BluetoothDevice.ACTION_FOUND.equals(action)
					|| BluetoothDevice.ACTION_ACL_CONNECTED.equals(action)) {
				Log.e("Adding device", "adding device");
				addDevice(device);
			} else if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(action)
					|| BluetoothDevice.ACTION_ACL_DISCONNECT_REQUESTED
							.equals(action)) {

				Log.e("removing device", "removing device");
				removeDevice(device);
			}

			// update the list regardless
			// updateListBasedOnDC();

			sendInternal();
		}
	};

	public void addDevice(BluetoothDevice device) {
		if (device.getName() == null) {
			return;
		}
		// if we do not, we get it
		// if (device.getName() != null) {
		Device d = new Device();
		d.setMAC(device.getAddress());
		d.setName(device.getName());
		SmartParkApplication.addToBTS(this.getApplicationContext(),d);

		// if I'm paired with this device and I'm connected, I will mark
		// this place as free
		removeFrom(d);
		//addTo();
	}

	public void removeDevice(BluetoothDevice device) {
		Device dev = null;
		if (device.getName() != null) {

			for (Device d : SmartParkApplication.getDevices()) {
				// System.out.println(entry.getKey() + "/" + entry.getValue());
				if (d.getMAC().equals(device.getAddress())) {
					dev = new Device();
					dev.setMAC(d.getMAC());
					dev.setName(d.getName());
					SmartParkApplication.removeFromBTS(this.getApplicationContext(),d);
					addTo();
					return;
				}
			}
		}
			

	}

	public void removeFrom(Device d) {

		if (SessionManager.getInstance(this.getApplicationContext())
				.getPairedDevice() == null) {
			Log.d("paired device is null", "paired is null");
			return;
		}
		if (!SessionManager.getInstance(this.getApplicationContext())
				.getPairedDevice().equals(d.getMAC())) {
			Log.d("paired device is not equal", "paired is not equal");
			return;
		}

		String url = "http://54.149.101.118/spots/leave?";
	//	url += ("bid=" + APIManager.encode64(d.getMAC()));
		url += ("lat=" + SmartParkApplication.getInstance().getLastLocation()
				.getLatitude());
		url += ("&lng=" + SmartParkApplication.getInstance().getLastLocation()
				.getLongitude());
		String s = SessionManager.getInstance(this.getApplicationContext())
				.getPairedDevice();
		url += ("&bid=" + APIManager.encode64(s));
		Log.d("url", url);

		Map<String, String> headers = new HashMap<String, String>();
		/*
		 * new GetDataWebTask Object o =
		 * SocketOperator.getInstance(SmartParkResponse.class)
		 * .getResponse(this.getApplicationContext(), url, headers);
		 */
		// Map<String, String> headers = APIManager.defaultSessionHeaders();

		new GetDataWebTask<RemoveBluetoothResponse>(
				(AsyncTaskCompleteListener) this, this.getApplicationContext(),
				RemoveBluetoothResponse.class, false).execute(url,
				MapUtils.mapToString(headers));

		// return (RemoveBluetoothResponse) o;
	}

	public void addTo() {
		// if I'm paired with this device and I'm disconnected, I will mark this
		// place as taken
		
		//park is mark space as no longer available
		//leave is mark space as available

		// if I'm paired with this device and I'm connected, I will mark
		// this place as free
		if (SessionManager.getInstance(this.getApplicationContext())
				.getPairedDevice() == null) {
			Log.d("paired device is null", "paired is null");
			return;
		}

		String url = "http://54.149.101.118/spots/park?";
		url += ("lat=" + SmartParkApplication.getInstance().getLastLocation()
				.getLatitude());
		url += ("&lng=" + SmartParkApplication.getInstance().getLastLocation()
				.getLongitude());
		String s = SessionManager.getInstance(this.getApplicationContext())
				.getPairedDevice();
		url += ("&bid=" + APIManager.encode64(s));
		Log.d("url", url);

		Map<String, String> headers = new HashMap<String, String>();
		/*
		 * new GetDataWebTask Object o =
		 * SocketOperator.getInstance(SmartParkResponse.class)
		 * .getResponse(this.getApplicationContext(), url, headers);
		 */
		// Map<String, String> headers = APIManager.defaultSessionHeaders();

		new GetDataWebTask<RemoveBluetoothResponse>(
				(AsyncTaskCompleteListener) this, this.getApplicationContext(),
				RemoveBluetoothResponse.class, false).execute(url,
				MapUtils.mapToString(headers));
	}

	public void sendInternal() {
		Intent intent = new Intent(SessionManager.NEW_DEVICE);
		LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
		// this.getApplicationContext().sendBroadcast(intent);
		Log.d("Sending interna", "sendinginternal");
	}

	protected void updateListBasedOnDC() {
		// reload the stuff
		/*
		 * String pairedDevice = SessionManager.getInstance(
		 * this.getApplicationContext()).getPairedDevice(); if (pairedDevice ==
		 * null) { return; } boolean hasPaired = false; for (Device d :
		 * SmartParkApplication.BTArrayList) { if
		 * (d.getMAC().equals(pairedDevice)) hasPaired = true; } if (hasPaired)
		 * { return; }
		 */
		/*
		 * Device d = new Device(); d.setMAC(mAC)
		 * SmartParkApplication.BTArrayList.add(object)
		 */
		// Log.d("We lost the device", "We lost");
	}

	public void off() {
		myBluetoothAdapter.disable();
		// text.setText("Status: Disconnected");

		Toast.makeText(getApplicationContext(), "Bluetooth turned off",
				Toast.LENGTH_LONG).show();
	}

	public void find() {
		if (myBluetoothAdapter.isDiscovering()) {
			// the button is pressed when it discovers, so cancel the discovery
			myBluetoothAdapter.cancelDiscovery();
		} 
			// SmartParkApplication.BTArrayList.clear();

			myBluetoothAdapter.startDiscovery();

	}

	/*public void list() {
		// get paired devices
		pairedDevices = myBluetoothAdapter.getBondedDevices();

		// put it's one to the adapter
		for (BluetoothDevice device : pairedDevices) {
			Device d = new Device();
			d.setMAC(device.getAddress());
			d.setName(device.getName());
			SmartParkApplication.BTArrayList.add(d);
		}

		Toast.makeText(getApplicationContext(), "Show Paired Devices",
				Toast.LENGTH_SHORT).show();

	}*/

	@Override
	public void onDestroy() {
		super.onDestroy();
		unregisterReceiver(bReceiver);
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void onTaskComplete(Object result) {
		// TODO Auto-generated method stub

	}

}