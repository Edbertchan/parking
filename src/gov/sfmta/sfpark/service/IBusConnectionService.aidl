package gov.sfmta.sfpark.service;

import gov.sfmta.sfpark.service.IConnectionChangeCallback;

interface IBusConnectionService {
	boolean isConnected();
	void startConnecting(String host);
	void disconnect();
	void registerCallback(IConnectionChangeCallback callback);
	void unregisterCallback(IConnectionChangeCallback callback);
}