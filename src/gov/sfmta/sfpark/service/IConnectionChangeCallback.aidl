package gov.sfmta.sfpark.service;

oneway interface IConnectionChangeCallback {
   void onConnectionStatusChanged(String host, String status);
}