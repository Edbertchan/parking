package gov.sfmta.sfpark;



import gov.sfmta.sfpark.core.Device;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;

import com.edbert.library.database.DatabaseCommandManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class SessionManager {
	// Shared Preferences
	static SharedPreferences pref;

	// Editor for Shared preferences
	static Editor editor;

	// Context
	Context _context;

	// Shared pref mode
	static int PRIVATE_MODE = 0;

	// Sharedpref file name
	public static final String PREF_NAME = "HIPPA_PIX_REFERENCE";

	private static SessionManager instance;

	// Default values
	public static final float DEFAULT_SEARCH_RADIUS = 20.0f;
	public static final String DEFAULT_STRING = "";
	public static final int DEFAULT_VALUE = -1;
	private static final String USER_ID = "USER_ID";

	public static final float SEARCH_DEFAULT_NUMERIC = -1;
	private static final String VAULT_ID = "VAULT_ID";
	private static final String LOGIN = "LOGIN";

	private static final String ACCESS_TOKEN = "ACCESS_TOKEN";
	private static final String SCHEMA_ID = "SCHEMA_ID";

	private SessionManager(Context c) {
		this._context = c;
		pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
	}

	public static synchronized SessionManager getInstance(Context c) {
		if (instance == null)
			instance = new SessionManager(c);
		return instance;
	}

	public static final String MAC_ADDR = "MAC_ADDR";

	public static final String NEW_DEVICE = "NEW_DEVICE";

	protected static final String DEFAULT_STRING_DISPLAY_LOCATION = "Current Location";

	public void setPairedDevice(String macAddress) {
		editor.putString(MAC_ADDR, macAddress);
		editor.commit();

	}
	public String getPairedDevice(){
		return pref.getString(MAC_ADDR, null);
	}

	//latlng
	public static final String LAT = "LAT";
	public void setCustomSearchLatitude(String latitude) {
		editor.putString(LAT, latitude);
		editor.commit();
	}
	public static final String LNG = "LNG";
	public void setCustomSearchLongitude(String longitude) {
		editor.putString(LNG, longitude);
		editor.commit();
	}
	public static final String LOCATION_NAME = "LOCATION_NAME";
	public void setCustomSearchLocationName(String locationName) {
		editor.putString(LOCATION_NAME, locationName);
		editor.commit();
	}
	
	public String getCustomSearchLocationName(){
		return pref.getString(LOCATION_NAME, null);
	}
	public String getCustomSearchLongitude(){
		return pref.getString(LNG, "0.0");
	}
	//Location, lat, lng
	public String getCustomSearchLatitude(){
		return pref.getString(LAT, "0.0");
	}
	public static final String CUSTOM_LOC = "CUSTOM_LOC";

	public static final String CURRENT_LOCATION = "Current Location";
	public void setHasCustomLocation(boolean b) {
		editor.putBoolean(CUSTOM_LOC, b);
		editor.commit();
		
	}
	public boolean hasCustomLocation() {
		return pref.getBoolean(CUSTOM_LOC, false);
	}
	
	public void saveListOfDevice(ArrayList<Device> al){
		
		 SharedPreferences appSharedPrefs = PreferenceManager
		  .getDefaultSharedPreferences(this._context);
		  Editor prefsEditor = appSharedPrefs.edit();
		  Gson gson = new Gson();
		  String json = gson.toJson(al);
		  prefsEditor.putString("MyObject", json);
		  prefsEditor.commit(); 
	}
	public ArrayList<Device> restoreFormerList(){
		SharedPreferences appSharedPrefs = PreferenceManager
			      .getDefaultSharedPreferences(this._context);
			      Editor prefsEditor = appSharedPrefs.edit();
			      Gson gson = new Gson();
			 String json = appSharedPrefs.getString("MyObject", "");
			 Type type = new TypeToken<List<Device>>(){}.getType();
			 return gson.fromJson(json, type);
	}
}