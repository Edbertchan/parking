package gov.sfmta.sfpark.core;

import gov.sfmta.sfpark.R;
import gov.sfmta.sfpark.SFFragmentActivity;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.MenuItem;

public abstract class ActivityContainer extends SFFragmentActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fragment_container);
		String fragmentClassName = getCanonicalName();
		displayFragment(fragmentClassName);

	}

	public abstract String getCanonicalName();

	protected Fragment f;

	// set up associated options menu
	private void displayFragment(String fragmentClassName) {
		Configuration config = getResources().getConfiguration();

		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction fragmentTransaction = fragmentManager
				.beginTransaction();

		Class fragmentClass;

		try {
			fragmentClass = Class.forName(fragmentClassName);
			f = (Fragment) fragmentClass.newInstance();

			fragmentTransaction.replace(R.id.frame_container, f);
			/*
			 * ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
			 * 
			 * ft.addToBackStack(backStateName); ft.commit();
			 * 
			 * fragmentTransaction.replace(android.R.id.content, f);
			 */
			fragmentTransaction.commit();
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (InstantiationException e) {

			e.printStackTrace();
		} catch (IllegalAccessException e) {

			e.printStackTrace();
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// should check to be safe since we'll have more than a back
		onBackPressed();
		return true;
	}
}