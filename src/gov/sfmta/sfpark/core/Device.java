package gov.sfmta.sfpark.core;

public class Device{
	String MAC, name;

	public String getMAC() {
		return MAC;
	}

	public void setMAC(String mAC) {
		MAC = mAC;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}