package gov.sfmta.sfpark.core;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class ClusterMask implements ClusterItem {
	private final LatLng mPosition;
	String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LatLng getmPosition() {
		return mPosition;
	}

	public ClusterMask(double lat, double lng) {
		mPosition = new LatLng(lat, lng);
	}

	@Override
	public LatLng getPosition() {
		return mPosition;
	}

	public float getColor() {
		// TODO Auto-generated method stub
		return color;
	}
	public void setColor(float color2) {
		color = color2;
	}
	float color = BitmapDescriptorFactory.HUE_BLUE;
}