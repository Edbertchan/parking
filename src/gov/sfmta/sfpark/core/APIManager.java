package gov.sfmta.sfpark.core;



import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import android.content.Context;
import android.util.Log;
import android.widget.EditText;

public class APIManager {
	

	private static String encode64UserPass(String username, String password) {
		return encode64(username + ":" + password);
	}

	public static String encode64(String authString) {
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		return authStringEnc;
	}


	public static String decode64(String str) {
		// Receiving side
		byte[] data = android.util.Base64.decode(str,
				android.util.Base64.DEFAULT);
		String text = "";
		try {
			text = new String(data, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return text;

	}


}