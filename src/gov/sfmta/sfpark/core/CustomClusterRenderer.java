package gov.sfmta.sfpark.core;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

public class CustomClusterRenderer extends DefaultClusterRenderer<ClusterMask>{

    public CustomClusterRenderer(Context applicationContext, GoogleMap map,ClusterManager<ClusterMask> mClusterManager) {
        super(applicationContext, map, mClusterManager);

    }
	@Override
	protected void onBeforeClusterItemRendered(ClusterMask item,
	        MarkerOptions markerOptions) {
		markerOptions.icon(BitmapDescriptorFactory
			        .defaultMarker(item.getColor()));
	}
}