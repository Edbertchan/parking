package gov.sfmta.sfpark.core;

import gov.sfmta.sfpark.R;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class ListOfDevicesAdapter extends ArrayAdapter<Device> {
	Context context;
	public ListOfDevicesAdapter(Context context, List<Device> users) {
		super(context, 0, users);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// Get the data item for this position
		Device user = getItem(position);
		// Check if an existing view is being reused, otherwise inflate the view
		 LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		 if (convertView == null) {
			 convertView = inflater.inflate(R.layout.device_info_layout, parent, false);
		    }
		
		TextView mac = (TextView) convertView.findViewById(R.id.MAC);
		TextView name = (TextView) convertView.findViewById(R.id.name);
		// Populate the data into the template view using the data object
		/*name.setVisibility(View.GONE);
		mac.setVisibility(View.GONE);*/
		mac.setText("mac");
		name.setText("name");
		if (name != null) {
			name.setVisibility(View.VISIBLE);
			name.setText(user.name);
		}
		if (mac != null) {
			mac.setVisibility(View.VISIBLE);
			mac.setText(user.MAC);
		}
		// Return the completed view to render on screen
		return convertView;
	}
}