package gov.sfmta.sfpark.requests;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import gov.sfmta.sfpark.SessionManager;
import gov.sfmta.sfpark.SmartParkApplication;
import gov.sfmta.sfpark.SmartParkContentProvider;
import gov.sfmta.sfpark.json.SmartParkResponse;
import gov.sfmta.sfpark.json.Spots;
import swipe.android.DatabaseHelpers.LocationDatabaseHelper;
import android.content.ContentValues;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.edbert.library.network.SocketOperator;
import com.edbert.library.network.sync.JsonResponseInterface;
import com.gabesechan.android.reusable.location.ProviderLocationTracker;

public class LocationParkingRequest extends SmartParkRequest<SmartParkResponse> {

	public LocationParkingRequest(Context c) {
		super(c);
	}

	String id;

	public LocationParkingRequest(Context c, String id) {
		super(c);
		this.id = id;
	}

	@Override
	public SmartParkResponse makeRequest(Bundle b) {
		String url = "http://54.149.101.118/spots?";
		
		double lat = SmartParkApplication.getInstance().getLastLocation()
				.getLatitude();
		double lng =SmartParkApplication.getInstance().getLastLocation()
				.getLongitude();
		
		if(SessionManager.getInstance(this.c).hasCustomLocation()){
		
			lat = Double.valueOf(SessionManager.getInstance(this.c).getCustomSearchLatitude());
			lng = Double.valueOf(SessionManager.getInstance(this.c).getCustomSearchLongitude());
		}
		url += "lat=" + String.valueOf(lat);
		url += "&";
		url += "lng=" + String.valueOf(lng);
		url += "&";
		url +="radius=7.0";
				
		Map<String, String> headers = new HashMap<String, String>();
		Object o = SocketOperator.getInstance(SmartParkResponse.class)
				.getResponse(c, url, headers);
		if (o == null)
			return null;

		return (SmartParkResponse) o;
	}

	@Override
	public Class getJSONclass() {
		// TODO Auto-generated method stub
		return SmartParkResponse.class;
	}

	public static int location = 0;

	float startPrice = 5.00f;
	float endPrice = 15.00f;
	int startSpot = 9;
	int endSpot = 30;

	int startTotalSpot = endSpot;
	int endTotalSpot = endSpot * 2;

	double rangeMin = 0;
	double rangeMax = .02;

	int size = 20;

	@Override
	public boolean writeToDatabase(Bundle b, Context context,
			SmartParkResponse o) {
		/*
		 * if(o == null || o.getSpots() == null){ Log.d("Null","null response");
		 * 
		 * return false; }
		 */
		/*
		 * for(int i = 0 ; i < o.getSpots().size();i++){ Log.d("Spots",
		 * String.valueOf(o.getSpots().get(i).getLat())); }
		 */
		// for now we will write random dummy stuff to the database

		SmartParkContentProvider.clearSingleTable(new LocationDatabaseHelper());

		if (o == null || o.getSpots().size() == 0) {
		//	generateRandomData(context);
		} else {
			populateData(context, o);
		}
		return false;
	}

	private void populateData(Context context, SmartParkResponse o) {

		List<ContentValues> mValueList = new LinkedList<ContentValues>();
		for (Spots s : o.getSpots()) {
			ContentValues cv = new ContentValues();
			cv.put(LocationDatabaseHelper.COLUMN_LOCATION_LATITUDE, s.getLat());
			cv.put(LocationDatabaseHelper.COLUMN_LOCATION_LONGITUDE, s.getLng());
			cv.put(LocationDatabaseHelper.COLUMN_LAST_TAKEN, s.getTimestamp());
			cv.put(LocationDatabaseHelper.COLUMN_DISTANCE, s.getDistance());
			mValueList.add(cv);
		}

		ContentValues[] bulkToInsert;
		bulkToInsert = new ContentValues[mValueList.size()];
		mValueList.toArray(bulkToInsert);

		c.getContentResolver()
				.bulkInsert(
						SmartParkContentProvider
								.contentURIbyTableName(LocationDatabaseHelper.TABLE_NAME),
						bulkToInsert);

	}

	private void generateRandomData(Context context) {
		double lat = ((SmartParkApplication) context.getApplicationContext()).location
				.getLatitude();
		double lng = ((SmartParkApplication) context.getApplicationContext()).location
				.getLongitude();

		// insert bulk
		List<ContentValues> mValueList = new LinkedList<ContentValues>();
		for (int i = 0; i < size; i++) {
			Random random = new Random(System.nanoTime());
			try {
				Thread.sleep(100);
			} catch (Exception e) {

			}
			double latOffset = rangeMin
					+ ((rangeMax - rangeMin) * random.nextDouble());
			int j = random.nextInt();
			if (j % 2 == 0) {
				latOffset *= -1;
			}
			double longOffset = rangeMin
					+ ((rangeMax - rangeMin) * random.nextDouble());
			j = random.nextInt();
			if (j % 2 == 0) {
				longOffset *= -1;
			}

			// Groups tempGroups = o.getGroups().get(i);
			ContentValues cv = new ContentValues();

			//cv.put(LocationDatabaseHelper.COLUMN_ID, System.currentTimeMillis());
		/*	cv.put(LocationDatabaseHelper.COLUMN_DATE,
					System.currentTimeMillis());

			cv.put(LocationDatabaseHelper.COLUMN_LOCATION_NAME, "location "
					+ location);*/
			Random random_gen = new Random(System.currentTimeMillis());
			float random_price = random_gen.nextFloat();
			float result_price = startPrice
					+ (random_price * (endPrice - startPrice));
		//	cv.put(LocationDatabaseHelper.COLUMN_PRICE_PER_HOUR, result_price);
			float random_taken = random_gen.nextFloat();
			int result_taken = (int) (startSpot + (random_taken * (endSpot - startSpot)));
		//	cv.put(LocationDatabaseHelper.COLUMN_SPOTS_TAKEN, result_taken);

			float random_total = random_gen.nextFloat();
			int result_total = (int) (startTotalSpot + (random_total * (endTotalSpot - startTotalSpot)));

			//cv.put(LocationDatabaseHelper.COLUMN_SPOTS_AVAILABLE, result_total);

			cv.put(LocationDatabaseHelper.COLUMN_LOCATION_LATITUDE, latOffset
					+ lat);
			cv.put(LocationDatabaseHelper.COLUMN_LOCATION_LONGITUDE, longOffset
					+ lng);
			cv.put(LocationDatabaseHelper.COLUMN_DISTANCE, 2.0);
			cv.put(LocationDatabaseHelper.COLUMN_LAST_TAKEN, 1428153027);
			mValueList.add(cv);
			location++;
		}

		ContentValues[] bulkToInsert;
		bulkToInsert = new ContentValues[mValueList.size()];
		mValueList.toArray(bulkToInsert);

		c.getContentResolver()
				.bulkInsert(
						SmartParkContentProvider
								.contentURIbyTableName(LocationDatabaseHelper.TABLE_NAME),
						bulkToInsert);
	}

}