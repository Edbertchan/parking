package gov.sfmta.sfpark.requests;

import gov.sfmta.sfpark.json.SmartParkResponse;
import android.content.Context;
import android.os.Bundle;

import com.edbert.library.network.sync.JsonResponseInterface;
import com.edbert.library.sendRequest.SendRequestInterface;

public abstract class SmartParkRequest<T extends SmartParkResponse> implements
		SendRequestInterface<T> {

	protected Context c;

	public SmartParkRequest(Context c) {
		this.c = c;
	}

	@Override
	public boolean executePostRetrieval(Bundle extras, Context c, T o) throws Exception{
		
		return true;
	}
}