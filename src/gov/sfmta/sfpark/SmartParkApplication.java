package gov.sfmta.sfpark;

import gov.sfmta.sfpark.core.Device;
import gov.sfmta.sfpark.requests.LocationParkingRequest;
import gov.sfmta.sfpark.service.BluetoothConnectionDCService;

import java.util.ArrayList;
import java.util.List;

import swipe.android.DatabaseHelpers.LocationDatabaseHelper;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;

import com.edbert.library.containers.VolleyCoreApplication;
import com.edbert.library.database.DatabaseCommandManager;
import com.edbert.library.sendRequest.SendRequestStrategyManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import java.lang.reflect.Type;

public class SmartParkApplication extends VolleyCoreApplication implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener,
		Application.ActivityLifecycleCallbacks,
		GoogleApiClient.ConnectionCallbacks,
		GoogleApiClient.OnConnectionFailedListener, LocationListener {

	public static final boolean DEVELOPER_MODE = false;
	public static boolean HAS_LOCATION = false;
	private GoogleApiClient mGoogleApiClient;

	public static final double lat = 34.07702;
	public static final double lng = -118.4468;

	private LocationRequest mLocationRequest;
	private static ArrayList<Device> BTArrayList = new ArrayList<Device>();
	SmartParkSyncHelper helper;
	// paypal stuff

	private FusedLocationProviderApi fusedLocationProviderApi = LocationServices.FusedLocationApi;

	public static final String DOER_ID = "DOER_ID";

	protected synchronized void buildGoogleApiClient() {
		mGoogleApiClient = new GoogleApiClient.Builder(this)
				.addConnectionCallbacks(this)
				.addOnConnectionFailedListener(this)
				.addApi(LocationServices.API).build();
	}
	

	@TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
	@Override
	public void onCreate() {
		super.onCreate();
		mInstance = this;
		buildGoogleApiClient();
		helper = new SmartParkSyncHelper(this);
		initImageLoader(getApplicationContext());
		registerDatabaseTables();
		DatabaseCommandManager.createAllTables(SmartParkContentProvider
				.getDBHelperInstance(this).getWritableDatabase());

		SendRequestStrategyManager.register(new LocationParkingRequest(this));

		super.registerActivityLifecycleCallbacks(this);
		startService(new Intent(this, BluetoothConnectionDCService.class));

		BTArrayList = SessionManager.getInstance(this).restoreFormerList();
		if(BTArrayList == null)
			BTArrayList = new ArrayList<Device>();
		// Log.d("Appliation restart", "Application restart");
	}

	Handler.Callback realCallback = null;
	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (realCallback != null) {
				realCallback.handleMessage(msg);
			}
		};
	};

	public Handler getHandler() {
		return handler;
	}

	public void setCallBack(Handler.Callback callback) {
		this.realCallback = callback;
	}

	@Override
	public void onTerminate() {
		super.unregisterActivityLifecycleCallbacks(this);
		super.onTerminate();

		Log.d("term", "term");
	}

	public Location getCurrentLocation() {
		return location;
	}

	private void registerDatabaseTables() {
		DatabaseCommandManager.register(new LocationDatabaseHelper());

	}

	public static void initImageLoader(Context context) {

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context).threadPriority(Thread.NORM_PRIORITY - 2)
				.denyCacheImageMultipleSizesInMemory()
				.diskCacheFileNameGenerator(new Md5FileNameGenerator())
				.diskCacheSize(50 * 1024 * 1024)
				// 50 Mb
				.tasksProcessingOrder(QueueProcessingType.LIFO)
				// Remove for release app
				.build();
		ImageLoader.getInstance().init(config);
	}

	public static DisplayImageOptions getDefaultOptions() {
		return new DisplayImageOptions.Builder().cacheInMemory(true)
				.cacheOnDisk(true).considerExifParams(true)
				.bitmapConfig(Bitmap.Config.RGB_565).build();
	}

	public SmartParkSyncHelper getSyncHelper() {
		return helper;
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnected(Bundle connectionHint) {

		mLocationRequest = LocationRequest.create();
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setInterval(1000); // Update location every second

		LocationServices.FusedLocationApi.requestLocationUpdates(
				mGoogleApiClient, mLocationRequest, this);

	}

	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityStarted(Activity activity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityResumed(Activity activity) {
		// mLocationClient.connect();
		mGoogleApiClient.connect();
		lastAct = activity;
	}

	public Activity lastAct;

	@Override
	public void onActivityPaused(Activity activity) {
		// mLocationClient.disconnect();

		mGoogleApiClient.disconnect();
	}

	@Override
	public void onActivityStopped(Activity activity) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onActivityDestroyed(Activity activity) {
		// TODO Auto-generated method stub

	}

	public static Location location;

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub

		location = arg0;
		HAS_LOCATION = true;
	}

	@Override
	public void onConnectionSuspended(int cause) {
		// TODO Auto-generated method stub

	}

	public Location getLastLocation() {
		return location;
	}

	public boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) this
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}

	// Volley
	private static SmartParkApplication mInstance;

	public static synchronized SmartParkApplication getInstance() {
		return mInstance;
	}

	public static void addToBTS(Context ctx, Device d) {
		BTArrayList.add(d);
		SessionManager.getInstance(ctx).saveListOfDevice(BTArrayList);
	}

	public static void removeFromBTS(Context ctx, Device d) {
		BTArrayList.remove(d);
		SessionManager.getInstance(ctx).saveListOfDevice(BTArrayList);
	}
	public static ArrayList<Device> getDevices() {
		return BTArrayList;
	}

}
