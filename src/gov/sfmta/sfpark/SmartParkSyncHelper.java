package gov.sfmta.sfpark;

import org.michenux.drodrolib.network.sync.AbstractSyncHelper;
import org.michenux.drodrolib.network.sync.SyncHelperInterface;

import android.content.Context;


public class SmartParkSyncHelper extends AbstractSyncHelper {
	String ACCOUNT_NAME;
	String AUTHORITY;
	String ACCOUNT_TYPE;
	public SmartParkSyncHelper(boolean enableSync, Context context) {
		super(enableSync, context);
	}

	public SmartParkSyncHelper(Context context) {
		super(context);
	}

	@Override
	public boolean equals(SyncHelperInterface e) {
		if (e instanceof SmartParkSyncHelper) {
			SmartParkSyncHelper messagesHelper = (SmartParkSyncHelper) e;
			if (messagesHelper.mAuthority.equals(this.mAuthority)
					&& messagesHelper.mAccount.equals(this.mAccount)) {
				return true;
			}
		}
		return false;
	}



	@Override
	public String getDefaultAccountName() {
		return c.getString(R.string.smartpark_accountName);
	}

	@Override
	public String getDefaultAccountType() {
		return c.getString(R.string.smartpark_accountType);
	}

	@Override
	public String getDefaultAuthority() {
		return c.getString(R.string.smartpark_sync_contentAuthority);
	}

}