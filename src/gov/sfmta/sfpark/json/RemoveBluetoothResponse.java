package gov.sfmta.sfpark.json;

import java.util.ArrayList;


import com.edbert.library.network.sync.JsonResponseInterface;


public class RemoveBluetoothResponse implements JsonResponseInterface {
	ArrayList<Spots> spots;
	@Override
	public boolean isValid() {
		return error == null;
	}

	public ArrayList<Spots> getSpots() {
		return spots;
	}

	public void setSpots(ArrayList<Spots> spots) {
		this.spots = spots;
	}

	private String error;
	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
}
   