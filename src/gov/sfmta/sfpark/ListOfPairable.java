package gov.sfmta.sfpark;

import gov.sfmta.sfpark.core.Device;
import gov.sfmta.sfpark.core.ListOfDevicesAdapter;

import java.util.List;

import android.app.ListActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class ListOfPairable extends SFListActivity {

	private TextView text;
	private List<Device> listValues;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		text = (TextView) findViewById(R.id.text);
		resetAdapter();
	}

	ListOfDevicesAdapter adapter;
	private BroadcastReceiver updateListOfDevicesReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d("RECIEVE", "RECEIVE");
			runOnUiThread(new Runnable() {
				public void run() {
				    resetAdapter();
				}
				});
			//resetAdapter();
			// put a notification here if needed
		}
	};

	public void resetAdapter() {
		adapter = new ListOfDevicesAdapter(this,
				SmartParkApplication.getDevices());
		
		for (Device d : SmartParkApplication.getDevices()) {
			Log.d("Device", d.getName());
		}
		this.getListView().invalidate();
		adapter.notifyDataSetInvalidated();
		setListAdapter(adapter);
		adapter.notifyDataSetChanged();
		this.getListView().invalidate();
		adapter.notifyDataSetInvalidated();
		if(SmartParkApplication.getDevices().size() == 0){
			this.getListView().setVisibility(View.GONE);
			text.setVisibility(View.VISIBLE);
		}else{
			this.getListView().setVisibility(View.VISIBLE);
			text.setVisibility(View.GONE);
		}
		
	}
	
	

	// when an item of the list is clicked
	@Override
	protected void onListItemClick(ListView list, View view, int position,
			long id) {
		super.onListItemClick(list, view, position, id);
		Device selectedItem = (Device) getListView()
				.getItemAtPosition(position);
		// String selectedItem = (String) getListAdapter().getItem(position);

		// text.setText("You clicked " + selectedItem + " at position " +
		// position);
		String s = "You have paired ";
		if (selectedItem.getName() == null) {
			s += selectedItem.getMAC();
		} else
			s += selectedItem.getName();
		Toast.makeText(this, s, Toast.LENGTH_LONG).show();
		SessionManager.getInstance(this).setPairedDevice(selectedItem.getMAC());
	}

	@Override
	public void onResume() {
		super.onResume();
		LocalBroadcastManager.getInstance(this).registerReceiver(
				updateListOfDevicesReceiver,
				new IntentFilter(SessionManager.NEW_DEVICE));
		
	
		// update maybe
		/*
		 * if (!mHasSync) { mTutorialSyncHelper.performSync(); }
		 */
	}

	@Override
	public void onPause() {
		super.onPause();

		LocalBroadcastManager.getInstance(this).unregisterReceiver(
				updateListOfDevicesReceiver);

	}
}