package gov.sfmta.sfpark;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import swipe.android.nearlings.googleplaces.GoogleParser;
import swipe.android.nearlings.googleplaces.GoogleParser.PlacesTask;
import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.edbert.library.network.AsyncTaskCompleteListener;
import com.example.deletableedittext.DeleteableEditText;

public class SearchActivity extends Activity implements
		AsyncTaskCompleteListener {
	DeleteableEditText place;
	EditText edt_input_place;
	ImageButton btn_delete_place;
	ImageButton btn_delete_item;

	ListView listOfPlaces;
	String[] from = new String[] { "description" };
	int[] to = new int[] { android.R.id.text1 };

	SimpleAdapter adapterWithItems;
	SimpleAdapter adapterWithoutItems;

	PlacesTask placesTask;
	Button cancel, search;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_activity);

		place = (DeleteableEditText) findViewById(R.id.search_places);

		cancel = (Button) findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				onBackPressed();
			}

		});
		search = (Button) findViewById(R.id.search);
		search.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {

				String location = edt_input_place.getText().toString();
				if (location.equals("")
						|| location
								.equals(SessionManager.DEFAULT_STRING_DISPLAY_LOCATION)) {
					location = SessionManager.DEFAULT_STRING;
					SessionManager.getInstance(SearchActivity.this).setHasCustomLocation(false);
					SearchActivity.this.finish();
				} else {

					new ProcessPlace(SearchActivity.this).execute(location);
				}/*
				 * SessionManager.getInstance(SearchActivity.this)
				 * .setSearchLocation(location);
				 */
				// finish();
			}

		});
		// EditText searchTo = (EditText)findViewById(R.id.medittext);
		edt_input_place = (EditText) place.findViewById(R.id.edt_input);
		btn_delete_place = (ImageButton) place.findViewById(R.id.btn_delete);
		listOfPlaces = (ListView) findViewById(R.id.search_places_list);

		edt_input_place.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				GoogleParser outer = GoogleParser.getInstance(s.toString(),
						SearchActivity.this);
				placesTask = outer.new PlacesTask(s.toString());
				placesTask.execute(s.toString());
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
				// btn_delete.setVisibility(edt_input.isFocused() && s.length()
				// > 0 ? View.VISIBLE : View.GONE);
			}

			public void afterTextChanged(Editable s) {
				btn_delete_place.setVisibility(edt_input_place.isFocused()
						&& s.length() > 0 ? View.VISIBLE : View.GONE);
			}
		});

		listOfPlaces.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				HashMap<String, String> description = (HashMap<String, String>) adapterWithItems
						.getItem(position);

				Iterator it = description.entrySet().iterator();
				while (it.hasNext()) {
					Map.Entry<String, String> pair = (Map.Entry) it.next();
					Log.d(pair.getKey(), pair.getValue());
					// System.out.println(pair.getKey() + " = " +
					// pair.getValue());

					// avoids a ConcurrentModificationException
				}
				String s = description.get("description");
				place.setText(s);
				// currentCoords.clear();
				// currentCoords.add(object)
			}
		});
		/*
		 * edt_input_place.setText(SessionManager.getInstance(SearchActivity.this
		 * ) .getSearchLocation()); String currentItem =
		 * SessionManager.getInstance(SearchActivity.this) .getSearchString();
		 * if (currentItem.equals(null) || currentItem.equals("")) { currentItem
		 * = SessionManager.DEFAULT_STRING_DISPLAY; }
		 * edt_input_search_item.setText(currentItem);
		 */
	}

	Set<String> currentCoords = new LinkedHashSet<String>();

	@Override
	public void onTaskComplete(Object o) {

		if (o instanceof List<?>) {
			List<HashMap<String, String>> resultOfGooglePlace = (List<HashMap<String, String>>) o;
			// Creating a SimpleAdapter for the AutoCompleteTextView
			adapterWithItems = new SimpleAdapter(getBaseContext(),
					resultOfGooglePlace, android.R.layout.simple_list_item_1,
					from, to);

			// Setting the adapter
			listOfPlaces.setAdapter(adapterWithItems);
		}else{
			//we're good
			String s  = (String ) o;
			String longitude = s.substring(0,s.indexOf(","));
			String latitude = s.substring(s.indexOf(" ") + 1, s.length());
			String location = edt_input_place.getText().toString();

			//SessionManager
			SessionManager sessionManager =SessionManager.getInstance(this);
			Set<String> setOf = new  CopyOnWriteArraySet<String>();
			setOf.add(location);
			setOf.add(latitude);
			setOf.add(longitude);
			sessionManager.setCustomSearchLatitude(latitude);
			sessionManager.setCustomSearchLongitude(longitude);
			sessionManager.setCustomSearchLocationName(location);
			sessionManager.setHasCustomLocation(true);
			this.finish();
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		if (SessionManager.getInstance(this).hasCustomLocation()) {
			String s= SessionManager.getInstance(this)
					.getCustomSearchLocationName();
			edt_input_place.setText(s);

		}else{
			edt_input_place.setText(SessionManager.DEFAULT_STRING_DISPLAY_LOCATION);
		}
	}

}